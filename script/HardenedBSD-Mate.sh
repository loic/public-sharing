#!/bin/sh
# Version 20220220 / BSD-2-Clause
# Copyright (c) 2021, Loic <loic.f@hardenedbsd.org>
# All rights reserved.

# Seul l'utilisateur root peut exécuter le script
if [ "$(id -u)" -ne "0" ]; then
	echo "Le script doit être exécuté en tant que root !" 1>&2
	exit 1
fi

# Introduction
echo
echo -e "\033[1;34;40mBienvenue dans le programme d'installation de HardenedBSD-Mate.\033[0m"
echo

while :; do
	read -p '(I)nstallation, (L)icence ou (S)ortir ? ' RINTRO

	case $RINTRO in
	[iI]*)	break;;
	[lL]*)	echo -e "\nScript sous licence BSD à deux clauses.\n" && RINTRO="";;
	[sS]*)	exit;;
	esac
done

cat <<__EOT
Début de l'installation...
__EOT

# Création des variables
ALPHA2MIN=$(grep "^keymap=" '/etc/rc.conf' | cut -d\" -f2 | cut -d\. -f1)
ALPHA2MAJ=$(echo $ALPHA2MIN | tr '[a-z]' '[A-Z]')
LANGUEUTF8=$(echo "$ALPHA2MIN"_"$ALPHA2MAJ".UTF-8)
UTILISATEUR=$(grep 1001 /etc/group | cut -d: -f1)

# Test de la présence d'un utilisateur avec un UID de 1001
if grep -q '1001' /etc/group 1>&2 ; then
	echo -e "\nPersonnalisation du système pour $UTILISATEUR."
else
	echo -e "\nCréation d'un utilisateur avec un UID de 1001."
	adduser -u 1001
fi

# Confirmer la proposition de la langue pour les locales UTF-8
read -p "Activer la locale $LANGUEUTF8 ? [O/n] " REPVAR
if [ "$REPVAR" = "N" ] || [ "$REPVAR" = "n" ] ; then
        echo -n "Entrer le nom de la locale UTF-8 à utiliser: "
        read LANGUEUTF8
        read -p "Utiliser la locale $LANGUEUTF8 ? [O/n] " REPVAR2
        if [ "$REPVAR2" = "N" ] || [ "$REPVAR2" = "n" ] ; then
			echo "Je reviendrai..."
			exit 0
        fi
        echo "$LANGUEUTF8 sera utilisé."
else
        echo "$LANGUEUTF8 sera utilisé."
fi

# MàJ verbeuse avec sauvegarde de l'ancien noyau
test -f /usr/sbin/hbsd-update && hbsd-update -C

read -p "Faire la mise à jour du système de base de HardenedBSD ? [N/o] " REPVAR3
if [ "$REPVAR3" = "O" ] || [ "$REPVAR3" = "o" ] ; then
	test -f /usr/sbin/hbsd-update && hbsd-update -V -K ancienHBSD
fi

# Installer le bureau Mate
env ASSUME_ALWAYS_YES=YES pkg bootstrap
pkg install -fy xinit xdg-user-dirs slim slim-themes
pkg install -y xorg || pkg install -y xorg-server xorg-drivers xorg-fonts
pkg install -y xf86-video-intel xf86-video-ati xf86-video-vesa xf86-video-amdgpu xf86-video-nv xf86-video-scfb
pkg install -y mate || pkg install -y mate-desktop
pkg install -y station-tweak thefish chicago95

# Permettre le démarrage de Mate
cat > /home/$UTILISATEUR/.xinitrc <<EOF
export LANG="$LANGUEUTF8"
export LC_ALL="$LANGUEUTF8"
export LC_MESSAGES="$LANGUEUTF8"
export LC_CTYPE="$LANGUEUTF8"
export LC_COLLATE="$LANGUEUTF8"
exec mate-session
EOF
chown $UTILISATEUR:$UTILISATEUR /home/$UTILISATEUR/.xinitrc

# Personnaliser SLIM
fetch https://git.hardenedbsd.org/loic/public-sharing/-/raw/main/theme/slim-hardenedbsd.tar.bz2
tar jxvf slim-hardenedbsd.tar.bz2
mv hardenedbsd/ /usr/local/share/slim/themes/hardenedbsd
sed -i -r "s/.*current_theme.*/current_theme hardenedbsd/g" /usr/local/etc/slim.conf
sed -i -r "s/.*simone.*/default_user $UTILISATEUR/g" /usr/local/etc/slim.conf

# Télécharger quelques fonds d'écran
mkdir -p /usr/local/share/backgrounds/hardenedbsd
fetch -o /usr/local/share/backgrounds/hardenedbsd/HardenedBSD-DarkBlue1.png https://git.hardenedbsd.org/loic/wallpapers/-/raw/master/Modified_by_Loic/HardenedBSD-DarkBlue1.png
fetch -o /usr/local/share/backgrounds/hardenedbsd/HardenedBSD-DarkBlue2.png https://git.hardenedbsd.org/loic/wallpapers/-/raw/master/Modified_by_Loic/HardenedBSD-DarkBlue2.png
fetch -o /usr/local/share/backgrounds/hardenedbsd/HardenedBSD-BlueSun.jpg https://git.hardenedbsd.org/loic/wallpapers/-/raw/master/Modified_by_Loic/HardenedBSD-BlueSun.jpg
sed -i -r "s/3C8F25/0B324A/g" /usr/local/share/glib-2.0/schemas/org.mate.background.gschema.xml

# Télécharger le fork de checksec.sh
fetch -o /usr/local/bin/hbsd-checksec https://git.hardenedbsd.org/loic/hbsd-checksec/-/raw/master/hbsd-checksec
chmod 555 /usr/local/bin/hbsd-checksec

# Permettre à l'utilisateur de lancer su, d'éteindre la machine et d'accéder au DRI
pw groupmod wheel -m $UTILISATEUR
pw groupmod operator -m $UTILISATEUR
pw groupmod video -m $UTILISATEUR

# Installer les logiciels les plus utilisés
pkg install -fy gimp cups cups-filters system-config-printer 
pkg install -fy gnumeric abiword 
pkg install -fy meld octopkg keepassxc
pw groupmod cups -m $UTILISATEUR

# Installer un navigateur par ordre de priorité et de disponibilité
pkg install -y firefox || pkg install -y firefox-esr || pkg install -y chromium

# Installer un lecteur multimédia par ordre de priorité et de disponibilité
pkg install -y vlc || pkg install -y smplayer

# Installer un client de messagerie par ordre de priorité et de disponibilité
pkg install -y claws-mail claws-mail-pgp || pkg install -y sylpheed || pkg install -y thunderbird

# Installer les utilitaires les plus utilisés
# A rajouter dans ~/.profile
# [[ $PS1 && -f /usr/local/share/bash-completion/bash_completion.sh ]] && source /usr/local/share/bash-completion/bash_completion.sh
pkg install -fy zip unzip unrar 7-zip cabextract
pkg install -fy sudo networkmgr seahorse gvfs 
pkg install -fy bash bash-completion fish nano htop
pkg install -fy sysinfo hardening-check automount
cp /usr/local/etc/automount.conf.sample /usr/local/etc/automount.conf

# Autoriser l'utilisateur à utiliser sudo (exemple pour octopkg)
echo "$UTILISATEUR ALL=(ALL) ALL" >> /usr/local/etc/sudoers

# Utiliser bash sur le profil utilisateur
chsh -s /usr/local/bin/bash $UTILISATEUR

# Configuration pour le wifi (à comparer avec networkmgr)
#wpa_passphrase "LAN" "Azertyui" > /etc/wpa_supplicant.conf
sysrc wlans_ath0="wlan0"
sysrc ifconfig_wlan0="WPA SYNCDHCP"

cat >/etc/wpa_supplicant.conf <<EOF
network={
        ssid="LAN"
        psk=c92487af1618e5b7063807302ee26bfb7fdd98d87fd0d9a6f6466c9a704c05a8
}
EOF

# Désactiver MPROTECT pour Firefox
hbsdcontrol pax disable mprotect /usr/local/lib/firefox/firefox
hbsdcontrol pax disable pageexec /usr/local/lib/firefox/firefox
hbsdcontrol pax disable mprotect /usr/local/lib/firefox/plugin-container

# Configuration du système en Français
# Pour firefox: https://addons.mozilla.org/addon/fran%C3%A7ais-language-pack/
cat >>/etc/login.conf <<EOF

french|French Users Accounts:\
       :charset=UTF-8:\
       :lang=$LANGUEUTF8:\
       :tc=default:
EOF

sysrc -f /etc/profile LANG="$LANGUEUTF8"
sysrc -f /etc/profile CHARSET="UTF-8"

cat > /usr/local/etc/X11/xorg.conf.d/10-keyboard.conf <<EOF
Section "InputClass"
        Identifier "Keyboard Defauls"
        #Driver "keyboard"
        MatchIsKeyboard "on"
        Option "XkbLayout" "fr"
EndSection
EOF

cat > /usr/local/etc/X11/xorg.conf.d/20-screen.conf <<EOF
#Section "Device"
        #Identifier "Card0"
        #Driver "vesa" # sinon tester avec: scfb, fbdev ou chips
#EndSection
EOF

echo "defaultclass = french" >> /etc/adduser.conf

# Activer les services pour le bureau Mate
sysrc moused_enable=YES dbus_enable=YES hald_enable=YES slim_enable=YES mate_enable=YES

# Charger le système de fichiers en espace utilisateur
pkg install -y fusefs-ext2 fusefs-jmtpfs fusefs-ntfs
sysrc kld_list="fusefs"

# Personnaliser les autres services
# Activer "ipv6_privacy=YES" si IPV6 est utilisé
sysrc sendmail_enable=NO clear_tmp_enable=YES background_dhclient=YES ipv6_network_interfaces=NO

# Optimiser le système pour un usage desktop
echo "kern.sched.preempt_thresh=224" >> /etc/sysctl.conf
echo "kern.ipc.shmmax=67108864" >> /etc/sysctl.conf
echo "kern.ipc.shmall=32768" >> /etc/sysctl.conf
echo "vfs.usermount=1" >> /etc/sysctl.conf
echo "kern.ipc.shm_use_phys=0" >> /etc/sysctl.conf

# Diminuer le timeout du menu du boot loader
sysrc -f /boot/loader.conf autoboot_delay=3

# Firewall à regarder avec "ipfw show"
sysrc firewall_enable=YES
sysrc firewall_type=workstation
sysrc firewall_myservices="22/tcp"
sysrc firewall_allowservices=any
sysrc firewall_quiet=YES
sysrc firewall_logdeny=YES
service ipfw start

# Activer l'autostart de networkmgr
mkdir -p "/home/$UTILISATEUR/.config/autostart/"
cat > /home/$UTILISATEUR/.config/autostart/networkmgr.desktop  <<EOF
[Desktop Entry]
Type=Application
Exec=networkmgr
Hidden=false
X-MATE-Autostart-enabled=true
Name[fr_FR]=networkmgr
Name=networkmgr
Comment[fr_FR]=networkmgr
Comment=networkmgr
X-MATE-Autostart-Delay=1
EOF
chown -R $UTILISATEUR:$UTILISATEUR "/home/$UTILISATEUR/.config/"
chmod 644 "/home/$UTILISATEUR/.config/autostart/networkmgr.desktop"

# Utiliser NTPdate pour synchroniser l'heure au démarrage sans utiliser le daemon NTP
sysrc ntpd_sync_on_start=NO
sysrc ntpdate_enable=YES

# procfs pour l'environnement de bureau MATE
if [ $(grep -q "/proc" "/etc/fstab"; echo $?) == 1 ]; then
	echo "proc            /proc           procfs  rw      0       0" >> /etc/fstab
fi

# Installer les ports
if [ ! -d "/usr/ports" ]; then
    pkg install -fy git-lite
    git clone --depth=1 --single-branch --branch hardenedbsd/main https://git.hardenedbsd.org/hardenedbsd/ports.git /usr/ports/
fi

# Spécifique à HardenedBSD
pkg install -fy secadm secadm-kmod

FHOSTS=$(sha256 /etc/hosts | awk '{print $4}')

cat > /usr/local/etc/secadm.rules <<EOF
secadm {
  integriforce {
    path: "/etc/hosts",
    hash: "$FHOSTS",
    type: "sha256",
    mode: "hard",
  }
}
EOF

sysrc secadm_enable=NO
#/usr/local/etc/rc.d/secadm start
#echo ERREUR-SECADM >> "/etc/hosts"
#grep SECADM /var/log/messages /etc/hosts

# Installer les Additions invité VirtualBox ou sinon les microcodes du CPU
if [ $(pciconf -lv | grep -i virtualbox 1>&2 ; echo $?) = "0" ]; then
	pkg install -fy virtualbox-ose-additions
	sysrc vboxguest_enable=YES vboxservice_enable=YES
	sysrc moused_enable=NO
else
       pkg install -fy devcpu-data drm-kmod
       service microcode_update enable
       service microcode_update start
fi

# Auditer les paquets
pkg audit -F

# Redémarrage
echo
while :; do
        read -p '(R)edémarrer ou (S)ortir ? ' RREBOOT

        case $RREBOOT in
        [rR]*)  reboot;;
        [sS]*)  exit;;
        esac
done
